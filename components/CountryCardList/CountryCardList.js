import CountryCard from "../CountryCard/CountryCard";

const CountryCardList = ({ countries }) => {
  return (
    <div>
      {countries.map(country => <CountryCard key={country.alpha3Code} country={country}/>)}
    </div>
  );
}

export default CountryCardList;
