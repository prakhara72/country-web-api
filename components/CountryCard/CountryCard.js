import Link from 'next/link';
import styles from './CountryCard.module.css';

const CountryCard = ({ country }) => {
  return (
    <div className={styles.country_card}>
      <img src={country.flag} alt={`${country.name}'s Flag`}/>
      <div className={styles.country_card_details}>
        <h3>{country.name}</h3>
        <p>{`Currency: ${country.currencies.map(currency => currency.name).join(', ')}`}</p>
        <div className={styles.country_card_details_buttongrp}>
          <a target="_blank" href={`https://www.google.com/maps/place/${country.name}`}>Show Map</a>
          <Link href={`/${country.alpha3Code}`}>Detail</Link>
        </div>
      </div>
    </div>
  );
}

export default CountryCard;
