import styles from './SearchBar.module.css';

const SearchBar = ({ filterCountryList }) => {
  // Gets triggered whenever the user types something in the searchbar
  const handleChange = e => {
    const search = e.target.value.toLowerCase();

    // filterCountryList changes the country list based on the parameter passed
    // Here we pass the current value in the searchbox
    filterCountryList(search);
  };

  // To check when user presses Enter on the searchbar
  const handleKeyPress = e => {
    if (!e) e = window.event;
    var keyCode = e.code || e.key;
    if (keyCode == 'Enter')
    {
      // Reset the value of the search bar
      e.target.value = '';
      // Remove focus from the search bar
      document.activeElement.blur();
      // This will give the illusion that the user has submitted the query
    }
  };

  return (
    <div className={styles.searchbar}>
      <input onKeyPress={handleKeyPress} onChange={handleChange} type="text" placeholder="Search countries"/>
      {/* Icon */}
      <svg aria-labelledby="title desc" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.9 19.7"><title id="title">Search Icon</title><desc id="desc">A magnifying glass icon.</desc><g className="search-path" fill="none" stroke="#848F91"><path strokeLinecap="square" d="M18.5 18.3l-5.4-5.4"/><circle cx="8" cy="8" r="7"/></g></svg>
    </div>
  );
}

export default SearchBar;
