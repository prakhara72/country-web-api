# Country Web API
A web app which lists all the countries and allows user to view their map on Google Maps and other details as well.

View deployed project [here!](https://country-web-api.vercel.app/)
