import Head from 'next/head';
import { useState } from 'react';
import styles from '../styles/Home.module.css';

import Header from '../components/Header/Header';
import SearchBar from '../components/SearchBar/SearchBar';
import CountryCardList from '../components/CountryCardList/CountryCardList';

export default function Home({ countries }) {
  const [countryList, setCountryList] = useState(countries);
  const filterCountryList = (search) => {
    setCountryList(countries.filter(country => country.name.toLowerCase().startsWith(search)));
  };
  return (
    <div className={styles.container}>
      <Head>
        <title>Country Web App</title>
      </Head>

      <Header text="Countries"/>
      <SearchBar filterCountryList={filterCountryList} />
      <CountryCardList countries={countryList}/>
    </div>
  );
};

export async function getStaticProps() {
  const res = await fetch(`https://restcountries.eu/rest/v2/all`);
  const countries = await res.json();

  return {
    props: {
      countries
    }
  };
};